package com.devcamp.jbr40.customerinvoiceapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerInvoice {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList <Invoice> invoices(){
        ArrayList <Invoice> invoiceList = new ArrayList<>();
        Customer customer1 = new Customer(11, "Duy", 10);
        Customer customer2 = new Customer(12, "Huong", 30);
        Customer customer3 = new Customer(13, "Tinh", 5);

        System.out.println("Customer1: " + customer1);
        System.out.println("Customer2: " + customer2);
        System.out.println("Customer3: " + customer3);

        Invoice invoice1 = new Invoice(23, customer1, 5000.0);
        Invoice invoice2 = new Invoice(24, customer2, 4000.0);
        Invoice invoice3 = new Invoice(24, customer3, 8000.0);

        System.out.println("Invoice1: " + invoice1);
        System.out.println("Invoice2: " + invoice2);
        System.out.println("Invoice3: " + invoice3);


        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);

        return invoiceList;
    }
}
